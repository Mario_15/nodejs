const express = require ('express');
const router = express.Router();

const registro = require('../model/task');

router.get('/', async (req, res) =>{
    const registra = await registro.find();

    res.render('index' ,{
        registra
    });
});

router.post('/add', async (req, res) =>{
    const tasks = new registro(req.body);
    await tasks.save();
    res.redirect('/');
});

router.get('/edit/:id',async (req, res)=> {
    const { id } = req.params;
    const registra = await registro.findById(id);
    res.render('edit',{
        registra
    }); 
});

router.post('/edit/:id', async (req, res)=>{
    const { id } = req.params;
    await registro.update({ _id: id}, req.body);
    res.redirect('/');

});

router.get('/delete/:id', async (req, res) =>{
  const { id } = req.params;
    await registro.remove({_id: id});
    res.redirect('/');
});

module.exports = router;